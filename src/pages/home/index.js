import Home from '@/pages/home/Home'
import Index from '@/pages/home/Index'

const routes = [
  {
    path: '/',
    component: Home,
    children: [
      {
        path: '',
        name: 'Index',
        component: Index,
        meta: { bodyClass: 'login' }
      },
    ]
  }
]

export default routes
