// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from '@/store'

// import styles
import '@/assets/sass/app.scss'

// import scripts
import 'jquery'
import 'bootstrap'

// Toasted
import Toasted from 'vue-toasted'
Vue.use(Toasted)

// moment.js
const moment = require('moment')
require('moment/locale/pt-br')
Vue.use(require('vue-moment'), { moment })

// Vue Progress Bar
import VueProgressBar from 'vue-progressbar'
const progressBarOptions = {
  color: '#007bff',
  thickness: '4px'
}
Vue.use(VueProgressBar, progressBarOptions)

Vue.config.productionTip = false

/* eslint-disable no-new */
export default new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
