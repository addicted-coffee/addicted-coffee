import Vue from 'vue'
import Router from 'vue-router'
import AppInit from './guards/AppInit'

import AuthRoutes from '@/pages/auth'
import HomeRoutes from '@/pages/home'

import Toasted from 'vue-toasted'
import vbclass from 'vue-body-class'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    ...AuthRoutes,
    ...HomeRoutes
  ],
  linkActiveClass: 'active'
})

router.beforeEach(AppInit)

Vue.use(Toasted, {
  router,
  position: 'bottom-right',
  duration: 5000,
  closeOnSwipe: true
})

Vue.use(vbclass, router)

export default router
